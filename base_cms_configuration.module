<?php
/**
 * @file
 * Code for the Base CMS Configuration feature.
 */

include_once 'base_cms_configuration.features.inc';

/**
 * Implements hook_preprocess_fieldset().
 *
 * Override the "CAPTCHA" fieldset label and change it to "Security".
 */
function base_cms_configuration_preprocess_fieldset(&$vars) {
  if (!empty($vars['element']['#title']) && $vars['element']['#title'] == t('CAPTCHA')) {
    $vars['element']['#title'] = t('Security');
  }
}

/**
 * Implements hook_form_alter().
 *
 * Add text to the top of all forms not using the admin theme
 * to let the user what marker indicates required fields.
 */
function base_cms_configuration_form_alter(&$form, &$form_state, $form_id) {
  global $theme;
  $admin_theme = variable_get('admin_theme');
  $required_field_count = base_cms_configuration_form_has_required_fields($form);
  $ignore_forms = array(
    'user_cancel_confirm_form',
    'user_login',
    'user_pass',
    'views_exposed_form',
  );
  drupal_alter('required_fields_notice_ignore_forms', $ignore_forms);
  if ($theme != $admin_theme && !empty($required_field_count) && !in_array($form_id, $ignore_forms)) {
    $required_fields_field = array(
      '#type' => 'markup',
      '#markup' => '<div class="messages--warning messages warning">Required fields are marked with a <span class="form-required fa fa-star">*</span></div>',
      '#weight' => -10,
    );
    if (stristr($form_id, 'webform_client_form')) {
      $form['submitted']['required_fields'] = $required_fields_field;
    }
    else {
      $form['required_fields'] = $required_fields_field;
    }
  }
}

/**
 * Determine whether or not a form has any required fields.
 */
function base_cms_configuration_form_has_required_fields(&$form_elements) {
  $required_count = 0;
  $children = element_children($form_elements);
  if (!empty($children)) {
    foreach ($children as $key) {
      if (!empty($form_elements[$key]['#required'])) {
        $required_count += 1;
      }
      $element_children = element_children($form_elements[$key]);
      if (!empty($element_children)) {
        $required_count += base_cms_configuration_form_has_required_fields($form_elements[$key]);
      }
    }
  }
  return $required_count;
}

/**
 * Implements hook_theme_registry_alter().
 */
function base_cms_configuration_theme_registry_alter(array &$theme_registry) {
  if (isset($theme_registry['form_required_marker']['function'])) {
    $theme_registry['form_required_marker']['function']
      = 'base_cms_configuration_form_required_marker';
  }
}

/**
 * Implements theme_form_required_marker().
 *
 * Apply a fontawesome star icon to required markers.
 */
function base_cms_configuration_form_required_marker(array $variables) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();
  $attributes = array(
    'class' => 'form-required fa fa-star',
    'title' => $t('This field is required.'),
  );
  return '<span' . drupal_attributes($attributes) . '>*</span>';
}
