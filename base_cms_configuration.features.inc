<?php
/**
 * @file
 * base_cms_configuration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function base_cms_configuration_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "linkit" && $api == "linkit_profiles") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function base_cms_configuration_image_default_styles() {
  $styles = array();

  // Exported image style: content_fit.
  $styles['content_fit'] = array(
    'label' => 'Content Fit',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 960,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: square_thumbnail.
  $styles['square_thumbnail'] = array(
    'label' => 'Square Thumbnail',
    'effects' => array(
      2 => array(
        'name' => 'imagefield_focus_scale_and_crop',
        'data' => array(
          'width' => 100,
          'height' => 100,
          'strength' => 'medium',
          'fallback' => 'smartcrop',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
